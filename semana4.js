var app =new Vue ({
    el:'.container',
    data:{
        candidatos:[
            {name:"Mr.Black", votos: 0},
            {name:"Mr.While", votos: 0},
            {name:"Mr.Pink", votos: 0},
            {name:"Mr.Brown", votos: 0},
        ]
    },

    computed:{
        mayor: function(){
            var candidatoelegido= this.candidatos.sort(function(a,b){
                if(b.votos < a.votos) return -1;
                else if (b.votos>a.votos) return 1;
                return 0;
                
               // return b.votos - a.votos;

            });
            return candidatoelegido[0];
        }
    },

    methods:{
        clear: function(){
            this.candidatos=this.candidatos.map(function(candidato){
                candidato.votos=0;
                return candidato;
            })

        }

    }
})